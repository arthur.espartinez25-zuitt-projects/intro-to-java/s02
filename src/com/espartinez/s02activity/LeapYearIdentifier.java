package com.espartinez.s02activity;

import java.util.Scanner;

public class LeapYearIdentifier {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter Year: \n");
        int year = input.nextInt();
        if((0 == year % 4) && (0 != year % 100) || (0 == year % 400)){
            System.out.println(year+" is a leap year");
        } else {
            System.out.println(year+" is a not leap year");
        }
    }
}
